/**
 * Copyright (c) 2016 21CN.COM . All rights reserved.<br>
 * <p>
 * Description: TimeNLP<br>
 * <p>
 * Modified log:<br>
 * ------------------------------------------------------<br>
 * Ver.		Date		Author			Description<br>
 * ------------------------------------------------------<br>
 * 1.0		2016年5月4日		kexm		created.<br>
 */
package com.baidu.util.time;

import com.baidu.util.time.nlp.TimeAnalyze;
import com.baidu.util.time.nlp.TimeNormalizer;
import com.baidu.util.time.nlp.festival.FestivalDayAnalyse;
import com.baidu.util.time.util.DateUtil;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.URL;
import java.util.Calendar;

/**
 * 测试类
 */
public class TimeTest {

    private static TimeNormalizer normalizer;
    private static FestivalDayAnalyse festivalDayAnalyse;
    private static DateTime baseTime;

    @BeforeClass
    public static void init() throws Exception {
        URL url = TimeNormalizer.class.getResource("/TimeExp.m");
        System.out.println(url.toURI().toString());
        normalizer = new TimeNormalizer(url.toURI().toString());
        normalizer.setPreferFuture(true);

        festivalDayAnalyse = FestivalDayAnalyse.getInstance();
    }

    @Test
    public void testClearCutTime() throws Exception {
        baseTime = new DateTime("2017-08-22T21:12:21");
        testTime("下午3点半看电影", baseTime.plusDays(1).withTime(15, 30, 0, 0));
        testTime("今晚10点半看电影", baseTime.withTime(22, 30, 0, 0));
        testTime("今晚7点半看电影", baseTime.withTime(19, 30, 0, 0));
        testTime("今天晚上12点看电影", baseTime.plusDays(1).withTime(0, 0, 0, 0));
        testTime("明早8点23分看电影", baseTime.plusDays(1).withTime(8, 23, 0, 0));
        testTime("明天下午3点半看电影", baseTime.plusDays(1).withTime(15, 30, 0, 0));
        testTime("明晚9点半看电影", baseTime.plusDays(1).withTime(21, 30, 0, 0));
        testTime("明晚11点看电影", baseTime.plusDays(1).withTime(23, 0, 0, 0));
        testTime("明晚20点看电影", baseTime.plusDays(1).withTime(20, 0, 0, 0));
        testTime("明天晚上12点看电影", baseTime.plusDays(2).withTime(0, 0, 0, 0));
        testTime("后天下午4点看电影", baseTime.plusDays(2).withTime(16, 0, 0, 0));
        testTime("星期五晚上8点看电影", baseTime.plusDays(3).withTime(20, 0, 0, 0));
        testTime("周五晚上7点看电影", baseTime.plusDays(3).withTime(19, 0, 0, 0));
        testTime("下周四晚8点看电影", baseTime.withDayOfMonth(31).withTime(20, 0, 0, 0));
        testTime("17号晚8点看电影", baseTime.withMonthOfYear(9).withDayOfMonth(17).withTime(20, 0, 0, 0));
        testTime("8月19号18点看电影", baseTime.withDayOfMonth(19).withTime(18, 0, 0, 0));
        testTime("今晚看电影", baseTime.withTime(18, 0, 0, 0));
        testTime("明早看电影", baseTime.plusDays(1).withTime(10, 0, 0, 0));
        testTime("明天中午看电影", baseTime.plusDays(1).withTime(11, 0, 0, 0));
        testTime("明天下午看电影", baseTime.plusDays(1).withTime(13, 0, 0, 0));
        testTime("后天早上看电影", baseTime.plusDays(2).withTime(10, 0, 0, 0));
        testTime("后天下午看电影", baseTime.plusDays(2).withTime(13, 0, 0, 0));
        testTime("17号晚上看电影", baseTime.withMonthOfYear(9).withDayOfMonth(17).withTime(18, 0, 0, 0));
        testTime("18号中午看电影", baseTime.withMonthOfYear(9).withDayOfMonth(18).withTime(11, 0, 0, 0));
        testTime("星期五晚上看电影", baseTime.plusDays(3).withTime(18, 0, 0, 0));
        testTime("下周一早上看电影", baseTime.withDayOfMonth(28).withTime(10, 0, 0, 0));
        testTime("下周二下午看电影", baseTime.withDayOfMonth(29).withTime(13, 0, 0, 0));
        testTime("明天10点一刻看电影", baseTime.plusDays(1).withTime(10, 15, 0, 0));
        testTime("10分钟后看电影", baseTime.plusMinutes(10));
        testTime("3个小时后看电影", baseTime.plusHours(3));
        testTime("下个月8号看电影", baseTime.plusMonths(1).withDayOfMonth(8).withTime(18,30,0,0));
        testTime("周五晚上7点一起去看电影", baseTime.withDayOfMonth(25).withTime(19, 0, 0, 0));
        testTime("下周末晚上7点一起去看电影", baseTime.withMonthOfYear(9).withDayOfMonth(3).withTime(19, 0, 0, 0));
    }

    @Test
    public void testFestivalDay() throws Exception {
        baseTime = new DateTime("2017-08-22T21:12:21");
        testTime("圣诞节看电影", baseTime.withMonthOfYear(12).withDayOfMonth(25)
                .withTime(18, 30, 0, 0));
        testTime("明年圣诞节看电影", baseTime.plusYears(1).withMonthOfYear(12).withDayOfMonth(25)
                .withTime(18, 30, 0, 0));
        testTime("元旦看电影", baseTime.plusYears(1).withMonthOfYear(1).withDayOfMonth(1)
                .withTime(18, 30, 0, 0));
        testTime("明年除夕看电影", baseTime.plusYears(2).withMonthOfYear(2).withDayOfMonth(4)
                .withTime(18, 30, 0, 0));
        testTime("小年看电影", baseTime.plusYears(1).withMonthOfYear(2).withDayOfMonth(8)
                .withTime(18, 30, 0, 0));
    }

    @Test
    public void testErrorCase() throws Exception {
        baseTime = new DateTime("2017-08-22T21:12:21");
        testTime2("周五晚上聚餐", 2017, 8, 25, 18, 0, 0);
        testTime2("今年端午的下午3点半", 2017, 5, 30, 15, 30, 0);
        testTime2("明年端午的下午3点半", 2018, 6, 18, 15, 30, 0);
        testTime2("端午的下午3点半", 2018, 6, 18, 15, 30, 0);
        testTime2("元旦的下午3点半", 2018, 1, 1, 15, 30, 0);
        testTime2("明年元旦的下午3点半", 2018, 1, 1, 15, 30, 0);
        testTime("每天早晨五点提醒我起床跑步", baseTime.plusDays(1).withTime(5, 0, 0, 0));
        testTime("度秘提醒我早晨九点五十去开会", baseTime.plusDays(1).withTime(9, 50, 0, 0));
        testTime("明天凌晨4点", baseTime.plusDays(1).withTime(4, 0, 0, 0));
        testTime2("明年情人节晚上7点买花", 2018, 2, 14, 19, 0, 0);
        testTime2("农历七月六号早晨8点提醒我给男朋友过生日", 2017, 8, 27, 8, 0, 0);
        testTime2("明年农历七月六号早晨8点提醒我给男朋友过生日", 2018, 8, 16, 8, 0, 0);
        testTime2("2017年元旦的下午3点半", 2017, 1, 1, 15, 30, 0);
    }

    @Test
    public void testErrorCase2() throws Exception {
        baseTime = new DateTime("2017-09-10T09:12:21");
        testTime2("星期一上午戴杏儿和康宁的东西", 2017, 9, 11, 8, 0, 0);
        testTime2("下周一，中午十二点约人吃饭" , 2017, 9, 11, 12, 0, 0);
        testTime2("下周五开会下午三点" , 2017, 9, 15, 15, 0, 0);
    }

    private void testTime(String text, DateTime exceptTime) {
        TimeAnalyze[] timeAnalyzes = normalizer.parse(text, DateUtil.toDefaultDateString(baseTime));
        if (exceptTime == null) {
            Assert.assertTrue(timeAnalyzes.length == 0);
            return;
        }

        String actualTime = DateUtil.formatDateDefault(timeAnalyzes[0].getTime());
        System.out.println(String.format("%s - %s", text, actualTime));
        Assert.assertEquals(DateUtil.formatDateDefault(exceptTime.toDate()), actualTime);
    }

    private void testTime2(String text, Integer... dateTimes) {
        DateTime exceptTime = new DateTime(dateTimes[0], dateTimes[1], dateTimes[2], dateTimes[3], dateTimes[4], dateTimes[5]);
        testTime(text, exceptTime);
    }

}
