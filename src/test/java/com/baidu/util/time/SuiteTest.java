package com.baidu.util.time;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by pengjian03 on 2017/6/20.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        TimeTest.class,
        TimeTestForScene.class,
        TimeTestForDuerOS.class
})
public class SuiteTest {
}
