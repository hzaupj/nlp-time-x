package com.baidu.util.time;

import com.baidu.util.time.nlp.TimeAnalyze;
import com.baidu.util.time.nlp.TimeNormalizer;
import com.baidu.util.time.util.DateUtil;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.URISyntaxException;
import java.net.URL;

/**
 * 场景相关测试
 * 
 * @author zhangsheng07
 * 
 */
public class TimeTestForScene {

    private static TimeNormalizer normalizer;

    @BeforeClass
    public static void init() throws Exception {
        URL url = TimeNormalizer.class.getResource("/TimeExp.m");
        System.out.println(url.toURI().toString());
        normalizer = new TimeNormalizer(url.toURI().toString());
        normalizer.setPreferFuture(false);
        normalizer.setHaveTime(false);
    }

    @Test
    public void testSceneTime() throws URISyntaxException {
        // 有明确时间点、无AM/PM关键字、有场景
//        testTime("明天十一点睡觉");
//        testTime("明天3点开会");
//
//        // 有明确时间点、有AM/PM关键字、有场景
//        testTime("明天晚上十一点吃夜宵");
//        testTime("明天晚上十点睡觉");
//        testTime("明天早上16点吃夜宵");
//
//        // 无明确时间点、无AM/PM关键字、有场景
//        testTime("明天聚餐");
//        testTime("50分开会");
//        testTime("明天起床");
//
//        // 无明确时间点、有AM/PM关键字、有场景
//        testTime("明天中午聚餐");
//        testTime("明天凌晨开会");

    }

    private void testTime(String content) {
        TimeAnalyze[] timeAnalyzes = normalizer.parse(content);
        for (TimeAnalyze timeAnalyze : timeAnalyzes) {
            String actualTime = DateUtil.formatDateDefault(timeAnalyze.getTime());
            System.out.println(String.format("%s - %s", content, actualTime));
            
        }
    }

}
