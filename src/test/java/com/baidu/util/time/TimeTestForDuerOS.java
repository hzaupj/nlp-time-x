package com.baidu.util.time;

import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.baidu.util.time.nlp.TimeAnalyze;
import com.baidu.util.time.nlp.TimeNormalizer;
import com.baidu.util.time.util.DateUtil;

/**
 * DuerOS相关测试
 * 
 * time-NLP：         返回时间表达式+明确的时间点<BR>
 * 接口业务层：       封装结果，按格式返回（时间表达式+明确的时间点）<BR>
 * DuerOS：           1、话术归一，再调用接口<BR>
 *                              2、判断时间表达式是精确到什么级别？（年|月|日|时|分秒）<BR>
 *                              3、判断[之以]?前 还是[之以]?前<BR>
 *                              4、拼接查询时间范围<BR>
 * 
 * @author zhangsheng07
 * 
 */
public class TimeTestForDuerOS {

    private static TimeNormalizer normalizer;

    // 基准时间
    private static DateTime baseTime = new DateTime("2017-09-15T12:00:00");

    @BeforeClass
    public static void init() throws Exception {
        URL url = TimeNormalizer.class.getResource("/TimeExp.m");
        normalizer = new TimeNormalizer(url.toURI().toString());
        normalizer.setPreferFuture(false);
        normalizer.setHaveTime(false);
        normalizer.setHandlingScene(false);
    }

    @Test
    public void testDuerOSTime() {
        testTime("帮我查询下前天的备忘内容", 2017, 9, 13, 0, 0, 0);
        testTime("看看我刚才的备忘内容", new DateTime());
        testTime("查下上周四的备忘", 2017, 9, 7, 0, 0, 0);
        testTime("昨天上午的备忘是什么", 2017, 9, 14, 8, 0, 0);
        testTime("查一下5月3号的备忘", 2017, 5, 3, 0, 0, 0);
        testTime("看一下昨天的备忘", 2017, 9, 14, 0, 0, 0);
        testTime("上周三备忘查询", 2017, 9, 6, 0, 0, 0);
        testTime("查询我两小时前记录的备忘", 2017, 9, 15, 10, 0, 0);
        testTime("查一下我上个月15号记的备忘", 2017, 8, 15, 0, 0, 0);
        testTime("查找我两个星期前记的备忘", 2017, 9, 1, 12, 0, 0);
        testTime("我想查询上周三的备忘", 2017, 9, 6, 0, 0, 0);
        testTime("帮我查查我昨天上午的备忘", 2017, 9, 14, 8, 0, 0);
        testTime("查询上月2号备忘的事情", 2017, 8, 2, 0, 0, 0);
        testTime("查查昨天早上做了的备忘", 2017, 9, 14, 8, 0, 0);
        testTime("看看今早的备忘内容", 2017, 9, 15, 7, 0, 0);
        testTime("查看刚刚备忘了什么", new DateTime());
        testTime("我想看上个月的备忘", 2017, 8, 1, 0, 0, 0);
        testTime("我要看上个月的备忘记录", 2017, 8, 1, 0, 0, 0);
        testTime("查看近半年内的备忘", 2017, 3, 16, 12, 0, 0);
        testTime("查询昨天的备忘记录", 2017, 9, 14, 0, 0, 0);
        testTime("查下我昨天的备忘录", 2017, 9, 14, 0, 0, 0);
        testTime("找一下我上星期记的备忘录", 2017, 9, 8, 0, 0, 0);
        // testTime("搜索我刚记的记事本", 2017, 9, 15, 0, 0, 0);
        testTime("翻一下我上星期的记录本", 2017, 9, 8, 0, 0, 0);
        testTime("查查上个月12号的备忘", 2017, 8, 12, 0, 0, 0);
        testTime("我想查一下上周记的备忘", 2017, 9, 8, 0, 0, 0);
        // testTime("我想知道11月份的备忘是什么", 2016, 11, 0, 0, 0, 0); // 没有年，默认今年，待确认
        testTime("帮我查一下2周前的备忘内容", 2017, 9, 1, 12, 0, 0);
        testTime("帮我查一下上周的备忘", 2017, 9, 8, 0, 0, 0);
        testTime("能不能帮我查一下5月15号的备忘内容", 2017, 5, 15, 0, 0, 0);
        testTime("查询一下今天上午的备忘", 2017, 9, 15, 8, 0, 0);
        testTime("查询我10分钟前记录的备忘内容", 2017, 9, 15, 11, 50, 0);
        testTime("帮我查询下上个星期三的备忘", 2017, 9, 6, 0, 0, 0);
        testTime("看一下我昨天的备忘", 2017, 9, 14, 0, 0, 0);
        testTime("帮我查一查4月2日的备忘", 2017, 4, 2, 0, 0, 0);
        testTime("搜索5个小时前的备忘", 2017, 9, 15, 7, 0, 0);
        testTime("看看昨天的备忘录", 2017, 9, 14, 0, 0, 0);
        testTime("我想知道三小时前我都备忘什么了", 2017, 9, 15, 9, 0, 0);
        testTime("看一下1小时前备忘录", 2017, 9, 15, 11, 0, 0);
        testTime("查询昨天10点备忘录", 2017, 9, 14, 10, 0, 0);
        testTime("查下备忘，明天的开会时间", 2017, 9, 16, 0, 0, 0);
        testTime("备忘录中后天的会议时间", 2017, 9, 17, 0, 0, 0);
        // testTime("查询备忘中的我的生日时期", 2017, 9, 15, 0, 0, 0);
        // testTime("查一下备忘中我的银行卡密码", 2017, 9, 15, 0, 0, 0);
        // testTime("查备忘中的手机密码", 2017, 9, 15, 0, 0, 0);
        // testTime("我的手机密码从备忘中查查", 2017, 9, 15, 0, 0, 0);
        // testTime("查询备忘，我的农行密码", 2017, 9, 15, 0, 0, 0);
        // testTime("查一下关于去机场几人的备忘", 2017, 9, 15, 0, 0, 0);
        // testTime("备忘查询，老婆回家日期", 2017, 9, 15, 0, 0, 0);
        // testTime("查一下备忘里什么时间开防汛会议", 2017, 9, 15, 0, 0, 0);
        // testTime("帮我查一下关于生日的备忘", 2017, 9, 15, 0, 0, 0);
    }

    private void testTime(String content, DateTime exceptTime) {
        TimeAnalyze[] timeAnalyzes = normalizer.parse(content, DateUtil.toDefaultDateString(baseTime));
        if (exceptTime == null) {
            Assert.assertTrue(timeAnalyzes.length == 0);
            return;
        }

        String actualTime = DateUtil.formatDateDefault(timeAnalyzes[0].getTime());
        System.out.println(String.format("%s - %s - %s - %s", content, DateUtil.formatDateDefault(exceptTime.toDate()),
                actualTime, analyze(content)));
        Assert.assertEquals(DateUtil.formatDateDefault(exceptTime.toDate()), actualTime);
    }

    private void testTime(String content, Integer...dateTimes) {
        DateTime exceptTime =
                new DateTime(dateTimes[0], dateTimes[1], dateTimes[2], dateTimes[3], dateTimes[4], dateTimes[5]);
        testTime(content, exceptTime);
    }

    /**
     * 判断时间级别（由DuerOS端自己处理）
     * 
     * @param exp
     * @return
     */
    private String analyze(String exp) {
        String yearRule = "年";
        String monthRule = "月";
        String weekRule = "周|星期";
        String dayRule = "(日|号)|(周|星期)[1-7]|(周|星期)[一|二|三|四|五|六|七]|(前天|昨天|后天)|([今明][天|早|晚])";
        String hourRule = "点|时|钟头";
        String minuteRule = "刻|分|秒";

        Pattern pattern = Pattern.compile(minuteRule);
        Matcher match = pattern.matcher(exp);
        if (match.find()) {
            return "minute";
        }

        pattern = Pattern.compile(hourRule);
        match = pattern.matcher(exp);
        if (match.find()) {
            return "hour";
        }

        pattern = Pattern.compile(dayRule);
        match = pattern.matcher(exp);
        if (match.find()) {
            return "day";
        }

        pattern = Pattern.compile(weekRule);
        match = pattern.matcher(exp);
        if (match.find()) {
            return "week";
        }

        pattern = Pattern.compile(monthRule);
        match = pattern.matcher(exp);
        if (match.find()) {
            return "month";
        }

        pattern = Pattern.compile(yearRule);
        match = pattern.matcher(exp);
        if (match.find()) {
            return "year";
        }

        return null;
    }

}
