package com.baidu.util.time.nlp.festival.strategy;

import com.baidu.util.time.nlp.festival.FestivalDayInfo;
import org.joda.time.DateTime;

public interface FestivalDayStrategy {
    DateTime getDateTime(String text, DateTime baseTime, FestivalDayInfo info);
}
