package com.baidu.util.time.nlp.festival;

/**
 * 公历
 */
public class Solar {
    public int solarDay;
    public int solarMonth;
    public int solarYear;

    public String toDataString() {
        return String.format("%s-%s-%s", solarYear, solarMonth, solarDay);
    }
}
