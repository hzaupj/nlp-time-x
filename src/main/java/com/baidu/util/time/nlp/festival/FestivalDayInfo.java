package com.baidu.util.time.nlp.festival;

public class FestivalDayInfo {

    private String name;
    private DayType dayType;
    private String date;

    public FestivalDayInfo(String text) {
        String[] textArray = text.split("\\|");
        dayType = DayType.valueOf(Integer.parseInt(textArray[0]));
        name = textArray[1];
        date = textArray[2];
    }

    public DayType getType() {
        return dayType;
    }

    public String getDateString() {
        return date;
    }

    public String getName() {
        return name;
    }
}
