package com.baidu.util.time.nlp.festival;

/**
 * 农历
 */
public class Lunar {
    public boolean isleap;
    public int lunarDay;
    public int lunarMonth;
    public int lunarYear;

    public String toDataString() {
        return String.format("%s-%s-%s", lunarYear, lunarMonth, lunarDay);
    }
}
