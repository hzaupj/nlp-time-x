package com.baidu.util.time.nlp.festival;

public enum DayType {
    EMPTY(0), // 0
    SOLAR(1), // 公历
    LUNAR(2), // 农历
    DAY_OF_WEEK(3), // 第几周的周几
    ;

    private int type;

    DayType(int type) {
        this.type = type;
    }

    public static DayType valueOf(int value) {
        for (DayType type : DayType.values()) {
            if (type.type == value) {
                return type;
            }
        }
        return DayType.EMPTY;
    }
}
