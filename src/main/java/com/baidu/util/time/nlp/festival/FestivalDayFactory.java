package com.baidu.util.time.nlp.festival;

import com.baidu.util.time.nlp.festival.strategy.DayOfWeekFestivalDayStrategy;
import com.baidu.util.time.nlp.festival.strategy.FestivalDayStrategy;
import com.baidu.util.time.nlp.festival.strategy.LunarFestivalDayStrategy;
import com.baidu.util.time.nlp.festival.strategy.SolarFestivalDayStrategy;

import java.util.HashMap;
import java.util.Map;

public class FestivalDayFactory {

    private static FestivalDayFactory factory = new FestivalDayFactory();
    private static Map<DayType, FestivalDayStrategy> strategyMap = new HashMap<>();

    static {
        strategyMap.put(DayType.LUNAR, new LunarFestivalDayStrategy());
        strategyMap.put(DayType.SOLAR, new SolarFestivalDayStrategy());
        strategyMap.put(DayType.DAY_OF_WEEK, new DayOfWeekFestivalDayStrategy());
    }

    private FestivalDayFactory() {
    }

    public static FestivalDayFactory getInstance() {
        return factory;
    }

    public static FestivalDayStrategy getFestivalDayStrategy(DayType type) {
        return strategyMap.get(type);
    }
}
