package com.baidu.util.time.nlp.festival.strategy;

import com.baidu.util.time.nlp.festival.FestivalDayInfo;
import com.baidu.util.time.util.DateUtil;
import org.joda.time.DateTime;

/**
 * 公历假日处理
 */
public class SolarFestivalDayStrategy implements FestivalDayStrategy {
    boolean isAutoPlusYear = false;
    @Override
    public DateTime getDateTime(String text, DateTime baseTime, FestivalDayInfo info) {
        if (text.indexOf(info.getName()) == -1) {
            return null;
        }
        if (baseTime == null){
            isAutoPlusYear = true;
            baseTime = DateTime.now();
        }
        String[] dateArray = info.getDateString().split("-");
        DateTime festivalDay = baseTime
                .withMonthOfYear(Integer.parseInt(dateArray[0]))
                .withDayOfMonth(Integer.parseInt(dateArray[1]));

        festivalDay = DateUtil.setDefaultTime(festivalDay);
        if(isAutoPlusYear && baseTime.isAfter(festivalDay)){
            festivalDay = festivalDay.plusYears(1);
        }
        return festivalDay;
    }
}
