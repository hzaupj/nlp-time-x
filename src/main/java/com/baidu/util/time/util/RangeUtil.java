package com.baidu.util.time.util;

import java.util.List;

import com.google.common.collect.Range;

/**
 * 区间相关逻辑
 * 
 * @author zhangsheng07
 * 
 */
public class RangeUtil {

    /**
     * 判断数字是否在区间（或多个区间）内
     * 
     * @param checkNum
     * @param ranges
     * @return
     */
    public static boolean isInRange(Float checkNum, List<Range<Float>> ranges) {
        for (Range<Float> range : ranges) {
            if (range.contains(checkNum)) {
                return true;
            }
        }

        return false;
    }

    /**
     * 取区间的交集
     * 
     * @param checkRanges
     * @param ranges
     * @return
     */
    public static Range<Float> intersection(List<Range<Float>> checkRanges, List<Range<Float>> ranges) {
        Range<Float> intersection = null;
        outerloop:
        for (Range<Float> range : ranges) {
            for (Range<Float> checkRange : checkRanges) {
                // 如果有相交，则取最大交集
                if (checkRange.isConnected(range)) {
                    intersection = checkRange.intersection(range);
                    break outerloop;
                }
            }
        }

        return intersection;
    }

}
