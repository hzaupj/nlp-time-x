package com.baidu.util.time.enums;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.collect.Range;

/**
 * <p>
 * AMPM的时间定义<BR>
 * 包含默认时间点、时间区间、匹配规则<BR>
 * <p>
 * 
 * @author zhangsheng07
 * 
 */
public enum AMPMTimeEnum {

    before_dawn(1f, build(Range.closed(0f, 12f)), "凌晨"), //
    early_morning(7f, build(Range.closed(0f, 12f)), "早晨|早间|晨间|今早|明早"), //
    morning(8f, build(Range.closed(0f, 12f)), "早上|上午"), //
    noon(12f, build(Range.closed(11f, 23f)), "中午|午间"), //
    afternoon(15f, build(Range.closed(13f, 23f)), "下午|午后|pm|PM"), //
    night(18f, build(Range.closed(0f, 3f), Range.closed(18f, 24f)), "晚上|夜间|夜里|今晚|明晚"); //

    /** 匹配所有AMPM的正则，如果枚举对象有修改，需要同时更新此处 **/
    public static final String RULE_OF_AMPM = "凌晨|早晨|早间|晨间|早上|上午|今早|明早|中午|午间|下午|午后|pm|PM|晚上|夜间|夜里|今晚|明晚";

    // 默认时间点
    private Float defaultTime = 0f;
    // AMPM的时间区间
    private List<Range<Float>> timeRanges = null;

    // 匹配的正则规则
    private String rule;

    private AMPMTimeEnum(Float defaultTime, List<Range<Float>> timeRanges, String rule) {
        this.setDefaultTime(defaultTime);
        this.setTimeRanges(timeRanges);
        this.setRule(rule);
    }

    /**
     * 构造时间区间，支持一个或多个区间
     * 
     * @param ranges
     * @return
     */
    @SafeVarargs
    private static List<Range<Float>> build(Range<Float>...ranges) {
        List<Range<Float>> arrayList = new ArrayList<Range<Float>>();

        for (Range<Float> range : ranges) {
            arrayList.add(range);
        }

        return arrayList;
    }

    public Float getDefaultTime() {
        return defaultTime;
    }

    public void setDefaultTime(Float defaultTime) {
        this.defaultTime = defaultTime;
    }

    public List<Range<Float>> getTimeRanges() {
        return timeRanges;
    }

    public void setTimeRanges(List<Range<Float>> timeRanges) {
        this.timeRanges = timeRanges;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    /**
     * 查询AMPM名称<BR>
     * 如果有多个AMPM，以首个AMPM为准<BR>
     * 
     * @param content
     * @return
     */
    public static String getAMPMName(String content) {
        Pattern pattern = Pattern.compile(AMPMTimeEnum.RULE_OF_AMPM);
        Matcher match = pattern.matcher(content);
        if (match.find()) {
            return match.group();
        }

        return null;
    }
    
    /**
     * 根据AMPM关键字获取枚举对象（正则匹配）
     * 
     * @param sceneName
     * @return
     */
    public static AMPMTimeEnum getEnumByAMPMName(String amPmName) {
        if (amPmName != null && !amPmName.isEmpty()) {
            for (AMPMTimeEnum rangeTimeEnum : AMPMTimeEnum.values()) {
                Pattern pattern = Pattern.compile(rangeTimeEnum.getRule());
                Matcher match = pattern.matcher(amPmName);
                if (match.find()) {
                    return rangeTimeEnum;
                }
            }
        }

        return null;
    }

}
