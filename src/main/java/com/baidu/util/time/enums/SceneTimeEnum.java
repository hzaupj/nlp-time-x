package com.baidu.util.time.enums;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.collect.Range;

/**
 * <p>
 * 场景的时间定义<BR>
 * 包含默认时间点、时间区间、匹配规则<BR>
 * <p>
 * 
 * @author zhangsheng07
 * @version
 * @since 2017年8月28日
 * 
 */
public enum SceneTimeEnum {

    get_up(6.5f, build(Range.closed(5f, 10f)), "起床"), //
    sleep(23f, build(Range.closedOpen(21f, 24f)), "睡觉"), //
    afternoon_nap(13f, build(Range.closed(12f, 14f)), "午觉|午休"), //
    meeting(15f, build(Range.closed(7f, 23f)), "开会|会议|组会"), //
    morning_meeting(9f, build(Range.closed(8f, 11f)), "晨会"), //
    dinner_together(18.5f, build(Range.closed(11f, 13f), Range.closed(17f, 20f)), "聚餐"), //
    appointment(18.5f, build(Range.closed(10f, 14f), Range.closed(17f, 21f)), "约会|看电影|去玩|去逛|逛街|逛夜市"); //

    /** 匹配所有场景的正则，如果枚举对象有修改，需要同时更新此处 **/
    public static final String RULE_OF_SCENE = "起床|睡觉|午觉|午休|开会|会议|组会|晨会|聚餐|约会|看电影|去玩|去逛|逛街|逛夜市";

    // 默认时间点
    private Float defaultTime = 0f;
    // 场景的时间区间
    private List<Range<Float>> timeRanges = null;
    // 匹配的正则规则
    private String rule;

    private SceneTimeEnum(Float defaultTime, List<Range<Float>> timeRanges, String rule) {
        this.setDefaultTime(defaultTime);
        this.setTimeRanges(timeRanges);
        this.setRule(rule);
    }

    /**
     * 构造时间区间，支持一个或多个区间
     * 
     * @param ranges
     * @return
     */
    @SafeVarargs
    private static List<Range<Float>> build(Range<Float>...ranges) {
        List<Range<Float>> arrayList = new ArrayList<Range<Float>>();

        for (Range<Float> range : ranges) {
            arrayList.add(range);
        }

        return arrayList;
    }

    public Float getDefaultTime() {
        return defaultTime;
    }

    public void setDefaultTime(Float defaultTime) {
        this.defaultTime = defaultTime;
    }

    public List<Range<Float>> getTimeRanges() {
        return timeRanges;
    }

    public void setTimeRanges(List<Range<Float>> timeRanges) {
        this.timeRanges = timeRanges;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    /**
     * 查询场景名称<BR>
     * 如果有多个场景，以首个场景为准<BR>
     * 
     * @param content 待匹配的内容
     * @return
     */
    public static String getSceneName(String content) {
        Pattern pattern = Pattern.compile(SceneTimeEnum.RULE_OF_SCENE);
        Matcher match = pattern.matcher(content);
        if (match.find()) {
            return match.group();
        }

        return null;
    }

    /**
     * 根据场景名称获取枚举对象（正则匹配）
     * 
     * @param sceneName
     * @return
     */
    public static SceneTimeEnum getEnumBySceneName(String sceneName) {
        if (sceneName != null && !sceneName.isEmpty()) {
            for (SceneTimeEnum sceneTimeEnum : SceneTimeEnum.values()) {
                Pattern pattern = Pattern.compile(sceneTimeEnum.getRule());
                Matcher match = pattern.matcher(sceneName);
                if (match.find()) {
                    return sceneTimeEnum;
                }
            }
        }

        return null;
    }

}
